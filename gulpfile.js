'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();

var paths = {
    srcCSS : './scss/style.scss',
    destCSS : './app/assets/css/',
    // js
    srcJS : [
                './node_modules/bootstrap/dist/js/bootstrap.min.js',
                './node_modules/jquery/dist/jquery.min.js',
                './node_modules/popper.js/dist/popper.min.js',
            ],
    destJS : './app/assets/js/'
}

gulp.task('sass', () => {
    return gulp
        .src(paths.srcCSS)
        .pipe(sass({ outputStyle: "compressed" }))
        .pipe(gulp.dest(paths.destCSS))
        .pipe(browserSync.reload({ stream: true }))
})

gulp.task('scriptJS', ()=>{
    return gulp
        .src(paths.srcJS)
        .pipe(gulp.dest(paths.destJS))
        .pipe(browserSync.reload({ stream: true }))
})

gulp.task('browserSync', ()=>{
    return browserSync.init({
        server:{
            baseDir: './app'
        }
    })
})

gulp.task('serve', gulp.parallel('browserSync', 'sass', ()=>{
        gulp.watch('./scss/**/*.scss', gulp.series('sass'));
        gulp.watch([
            './app/*.html',
            './app/assets/img/*.*',
            './app/assets/js/*.js'
        ], browserSync.reload);
        gulp.task('scriptJS');
}));